---
title: "Payment Failures"
linkTitle: "Payment Failures"
weight: 400
mermaid: true
date: 2022-12-14


description: >
 As a veterinary clinic user, review Auto-ship initiated home delivery orders in a payment failure status from PIMS.
---

One way to enhance a practice’s revenue is to give them the option to answer questions about client’s orders that appear to be stuck in the cogs of automation. You can now embed a tool in your application to give them access to view potential stoppages that might be due to credit card changes upon the client’s account.

By clicking edit you can help a customer edit their payment method.
![Example of the Payment Failure Edit](../../../en/docs/images/FailedPayments/EditPayment.png)

### Payment Failures
[Payment Failures - POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/payment_failures/post-url-orders-paymentfailure)

{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    Veterinary User ->> Your System: Click button
    Your System ->> Vetsource: Invoke API
    Vetsource ->> Your System: Return URL
    Your System ->> Veterinary User: Display page in iframe or browser tab
	Vetsource ->> Veterinary User: List of payment failure displayed
{{< /mermaid >}}
