---
title: "Order Integration"
linkTitle: "Order Integration"
weight: 400
mermaid: true
date: 2022-12-14


description: >
  As a veterinarian user, locate a Client and open the prescription writing tool with the customer’s information pre-populated from the Practice Information Management System.
---

Send customer information from the Practice Information Management System (PIMS) to Vetsource's prescription writing tool and either retrieve an existing or create a new customer’s record in a state to begin writing prescriptions.

### Client Checkout Sequence

{{< mermaid >}}
graph LR;
    A[Place New Order]
    A --> B[Create Prescription]
    B --> C[Check Out]
    C --> D[Order Receipt]
{{< /mermaid >}}<br>


**Placing a New Order**

When an order is triggered by the PIMS system the user will be redirected to ScriptRight interface with Customer information pre-populated. This feature enhances the veterinarian experience by eliminating duplicate data entry or waiting for delayed client data loads. Within this interface you can select from Vetsource’s Home Delivery product options.

**Creating Prescription for Order**

Your next step is selecting the Strength and Packaging options. Note for many products these will be defaulted due to calculations based off the current pet weight.

Then begin adding usage instructions and special notes for the pharmacists. Note that for many products there will be default usage instructions for convenience. If the veterinarian wishes to override the default with special instructions they may.

**Checking Out a New Order**

After all products are added to the client’s cart the veterinarian (or their staff) may check the client out in the procedure room, or the order can be saved for later to be completed at the front desk upon checkout.

**Order Receipt**

Finally you can print an order receipt to hand to your client.

**How you can Enable this in your application**

To enable in your system, you must provide a JSON string containing client, address, and pet information and make an API call to Vetsource. Your system will then programmatically invoke the URL by either sending it to the default browser or an iframe. Once the URL is invoked, the Vetsource ScriptRight system will attempt to locate the customer in Vetsource if they previously exist and will update any new information. If the customer is new, they will be added to the Vetsource system based off of the information provided. The ScriptRight application will then be in a ready state for entering new prescriptions and order information for that client and their pets.

### Cart Creation Sequence
[Cart - POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/scriptright/post-url-client-scriptright)
{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    Veterinary User ->> Your System: Click button
    Your System ->> Your System: Create JSON String
    Your System ->> Vetsource: Send JSON string in API Call
    Vetsource ->> Your System: Return URL
    Your System ->> Vetsource: Invoke URL
	Vetsource ->> Veterinary User: Data displayed in ScriptRight
{{< /mermaid >}}

### Important Information:
- The patient to appear selected upon entry into the Vetsource system will be marked with “selected” = true, in the JSON payload.
- Additional patients may be added and marked either as “inactive” = true or false, but these must be marked “selected” as false. Note that marking a pet as inactive = true will result in autoshipments for that pet being canceled on their next scheduled order.
- Patient weight must be submitted in pounds. If another unit is provided it should be converted before placement in the JSON body.


Below is an example of what your users could see in the Home Delivery checkout Process.

![Example of Saved Orders](../../../en/docs/images/SavedOrders/SavedOrders.png)

### Client Checkout
[Checkout - POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/checkout/post-url-orders-awaitingcheckout)

{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    Veterinary User ->> Your System: Click button
    Your System ->> Vetsource: Invoke API
    Vetsource ->> Your System: Return URL
    Your System ->> Veterinary User: Display page in iframe or browser tab
	Vetsource ->> Veterinary User: List of ScriptRight orders ready for checkout displayed
{{< /mermaid >}}