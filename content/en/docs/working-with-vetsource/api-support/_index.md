---
title: "Support"
linkTitle: "Support"
weight: 200
date: 2024-05-31
description: >
  How to get help
---

During development and implementation feel free to contact your designated [Technical Program Manager](../contacts).

Before reaching out please try the following to troubleshoot:
* View the exception being thrown. Usually this is a very self-explanatory explanation and will solve your issue.
* Presenting exceptions to the end user may have prevented you from requesting support.


In order for us to assist you as quickly as possible, we request that you supply us with the following information via your designated communication channel:

* General description of the issue
* Steps to reproduce
* The endpoint and payload for your request
* Status codes and JSON for any responses
* The exception message you are receiving
* The time of the issue
* A curl statement will greatly aid in troubleshooting
