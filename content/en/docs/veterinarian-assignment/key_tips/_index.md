---
title: "Important Implementation Points"
linkTitle: "Important Implementation Points"
weight: 170
mermaid: true
date: 2024-07-19 

description: >
  Helpful hints for implementing prescription management
---
You will probably first want to work on the [staff synchronization](https://vetsource-platform.gitlab.io/pim/docs/staff-management/). This is what keeps the providers in PIMS in sync with the provider records in Vetsource. For existing vetsource practices when the integration is enabled you will want to map the vetsource vets to the PIMS vet records. To do this you can perform a [GET staff call](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/get-staff) on the practice. During development we recommend that you work with your designated TPM to set some vet records up in the vetsource system so you can prepare to do this. During the initial [GET call](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/get-staff) there will be records with no staffId and you will want to get that set properly as that is required for any prescribing in the vetsource system. Using the staffApiId (which is a unique id in vetsource) you can perform a [PUT call](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/put-staff) to update common records. After the initial synchronization you would use the [PUT with the staffId](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/put-staff) from your system to do updates. You may want to periodically check for new staff records in vetsource as a clinic could add them through our system. You can add new veterinarians to our system using the [POST endpoint](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/post-staff-v2).


* itemType and orderItemApiId are important for designating order items to be approved.
* staffId/staffApiId is synonomous with the vetId/vetApiId as long as the staff has veterinarian = true.