---
title: "Prescription Management - Vet Assignment"
linkTitle: "Prescription Management - Vet Assignment"
weight: 400
mermaid: true
date: 2024-07-19


description: >
  As a veterinary staff user, assign ownership to order items that are awaiting approval so they can be placed in a specific veterinarian's queue.
---

### Key User Flows
* A staff could assign order items awaiting approval to assist busy veterinarians in seeing only the items for review that they are responsible for.
* A system could automate assigning order items based which veterinarian the client last saw at a practice.
* Staff can unassign/reassign items due to a staffing change so items don't get lost that need attention.

### Key Features
* Multi-select items and reassign with a single API call

### Implementation
You will need to know which active staff are available to offer an assignment list. That can be performed by using the [GET /staff](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/get-staff) endpoint.
You will also need a list of all the order items awaiting approval as found in the [GET /orders/awaitingapproval](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/get-orders-awaitingapproval) endpoint.
Compile a list of items to reassign noting the itemType and orderItemApiId's and compose them into a JSON list 
Using either the staffApiId (Unique Vetsource ID) or the staffId (ID from the PIMS) from the /staff endpoint send the payload as found in the example on the [PUT approval/assign](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/put-approval-assign) endpoint.

To unassign, compile the desired list as above but do according to the [PUT /approval/unassign](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/put-approval-unassign) endpoint.



