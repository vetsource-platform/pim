---
title: "Getting Started"
linkTitle: "Getting Started"
weight: 200
date: 2024-08-02
description: >
  Where and how to begin
---

Before using the Vetsource Platform APIs we need to enable test environment access for you.  Please contact [Technical Program Manager](../working-with-vetsource/contacts) to get started.

### Test environments

Vetsource will create a test environment for you to access during initial development. This will allow you to develop and test your code without creating real orders.

Your assigned Technical Program Manager will provide the following:

1. URL(s) to be used for test system access
1. Authorization token to provide access to test API endpoints
1. A practiceApiId value for a test practice in the test system
1. During the development and beta periods, your designated TPM will be your point of contact for second tier support. After a commonly determined period (post-beta) we will transition any tiered level support to Vetource support.
1. Note that anywhere a species list is selectable in the integrating system it must map to the following list:
   ```
   CANINE
   FELINE
   EQUINE
   OTHER
   FISH
   ARACHNID
   RABBIT
   AVIAN
   RODENT
   AMPHIBIAN
   BOVINE
   INSECT
   BAT
   GOAT
   REPTILE
   MARSUPIAL
   MUSTELID
   OVINE
   PORCINE
   PRIMATE
   ```

### What will I need to go live with our implementation?
* Before Vetsource will provide you a production token we (the Technical Program Manager, Product Owner, and Business Development) will want to see a demonstration of the proposed implementation.
* We will want to be provided with a sandbox which is connected with the test system provided above.
* We will also record professional videos to aid clients in understanding how the integration works. These will be shared with your product or marketing team for approval before posting in our online resources. If so desired you may post these in any help sections of your application. We do ask that any help articles or media you create for the integration be ran by our product and marketing team to confirm we align on messages and functionality.

### Important points to note
* We consider the PIMS system the source of truth for staff, client, client address, and pet records (although those things can be updated and changed in our system by providers using our Practice Home system, and by clients using our hosted e-commerce platform.)
* We do not write back staff, client, and pet records that get changed in our systems. You can however request the state of those records using our [GET staff](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/get-staff) and [GET client](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/get-client) endpoints. 
* It is very important to pay attention to exceptions. We cannot say this enough. We give very detailed messages which will help your end users fix their issues. Often you can take our exceptions and show them back verbatim to the end user and they can fix their own issues. This will greatly aid in lessening support calls for both of our organizations. Most, but not all, of those exceptions are detailed in our documentation, but know that as our system evolves we reserve the right to provide new exceptions. We recommend that you keep a log history of API calls for your own triage, even if just temporarily. Most of the time this will greatly aid your support and dev team.
* The GET calls for our system have a lot of detailed information you will need to aid your end users. It is a good idea to familiarize yourself with all of those elements and ask your designated TPM questions if you don't understand something.