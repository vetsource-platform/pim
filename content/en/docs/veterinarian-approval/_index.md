---
title: "Prescription Management"
linkTitle: "Prescription Management"
weight: 400
mermaid: true
date: 2024-05-31


description: >
  As a veterinary practice user, review and approve or deny prescriptions for Home Delivery orders directly from PIMS.
---

### Key User Flows
* Approve or deny refill requests from the practice’s e-commerce platform, compounding, and 3rd party retailers from within the system the veterinarians natively work in without the need to swivel-chair to another platform
* Automate appointment requests when denials are made for reasons like “patient not seen”, or when labs need to be performed before prescriptions can be prescribed

### Key Features
* Auto-populate details like default prescription labels, expiration dates, and maximum refill quantities to improve efficiencies
* Informative error responses directing the provider in exactly what they need to do to get a prescription approved
* Pre-selected denial reasons customized to request originations
* Instant access to key catalog information like ingredients or drug interactions, allowing the provider to give the best care possible

### Implementation
There are two options for managing the orders awaiting approval queue.

The quick development option allows you to send in a simple API call and receive back a URL that takes you directly to our Practice Home queue. You can display this to your end user in an iframed or separate browser window.

The second option is a bit more involved but gives you more flexibility by being able to control your own user interface. This gives you the ability to put the vetsource approval tools in the place where your users expect them within your native environment.

### Order Counts
Retrieve real time counts for orders awaiting checkout and orders awaiting approval for display in your application. This can be added to a link, button or notification flag to alert veterinarians and technicians that there is work awaiting their attention in the Vetsource application.

{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    Veterinary User ->> Your System: Click button
    Your System ->> Vetsource: Invoke API
    Vetsource ->> Your System: Consume data for display
{{< /mermaid >}}

### Example
See the [Order Counts - GET API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/counts/get-orders-counts) for the counts endpoint. Using this API you can return the current status of the veterinarian approval queue and the front desk checkout queue.

```
{
    "awaitingApprovalCount": 14,
    "awaitingCheckoutCount": 12
}
```
Display the number in your application with a link or button directly to the queue (See Client Checkout and Approval Queue). This will aid the veterinary user in being aware of actions that need to be taken in Vetsource's application directly from your application.

### Corporate and Buy Groups Example

See the [Corporate or Buying Groups Unapproved Order Counts - GET API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/counts/get-unapproved-orders-counts) for the counts endpoint. Using this API you can return the current status of the veterinarian approval queue for a corporate or buying group.

Here is an example of the JSON body returned:

```
[
  {
    "practiceId": "749",
    "practiceApiId": "aab27a2d-601f-4f57-841d-9458e38b6da7",
    "hospitalRef1": "4567",
    "hospitalRef2": "XYZ",
    "awaitingApprovalCount": 11
  },
  {
    "practiceId": "869",
    "practiceApiId": "f5dad3f1-c76d-466e-956d-ae54d79bef19",
    "hospitalRef1": "1234",
    "hospitalRef2": "ABD",
    "awaitingApprovalCount": 4
  }
]
```

### Approval

{{< mermaid >}}
graph TD;
    A[Click on order counts link or button]
    A --> B[Review Order Line Item]
    B --> C{Approve or Deny?}
    C -->|Approve| D[Order Item Approved]
    C -->|Denied| E[Order Item Denied]
    D --> F{Is Last Item?}
    F --> |No| B[Review Order Line Item]
    F --> |Yes| G[Order Processed]
{{< /mermaid >}}<br>

**Veterinarian Approve/Deny**

### Quick Development Option - Veterinarian Approve/Deny Sequence
[Rx Approval - POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/post-url-orders-awaitingapproval)

{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    Veterinary User ->> Your System: Click button
    Your System ->> Vetsource: Invoke API
    Vetsource ->> Your System: Return URL
    Your System ->> Veterinary User: Display page in iframe or browser tab
	Vetsource ->> Veterinary User: List of orders ready for approval displayed
{{< /mermaid >}}

### Best Development Option - Veterinarian Approve/Deny Sequence

The following API's allow you to completely embed the prescription approval experience in your application without having to iframe or send users to a browser window.

[Approval Queue - GET API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/get-orders-awaitingapproval)

When using the GET API, note the maximumExpirationDate, usageInstructions and defaultRefills values to preset values for the approval user interface you will design. 

Also note the denialReasons which are different based on itemType (e-Commerce, 3rdPartyOrigination, ScriptRight) you will need to present for selection in your denial user interface. If the denial reason selected is simply, "Other" you may present the user with a free form text field limited to 255 characters and send that in the denialReason for the deny payload. Note that these values are dynamic and should not be hard coded in your system as they may change at Vetsource's discretion. Use the [Single Item retrieval](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/get-orderitem-awaitingappral) call to populate default values.

[Rx Approval - POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/post-approve-rx)

Note: Pay close attention to 400 and 500 response errors. See the API docs for examples on the API calls. Often this will instruct the user on steps to resolve issues.

[Rx Denial - POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/post-deny-rx)

{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    Veterinary User ->> Your System: Click button
    Your System ->> Vetsource: Invoke GET API
    Vetsource ->> Your System: Return List of Order Items Awaiting Veterinary Action
    Your System ->> Veterinary User: Display Order Items
	Veterinary User ->> Your System: Click Approve or Deny button
    Your System ->> Vetsource: Make API call for specific item
	Your System ->> Veterinary User: Display Appropriate User Interface
	Veterinary User ->> Your System: Enter info and submit
	Your System ->> Vetsource: Invoke appropriate approve or deny POST API
	Vetsource ->> Your System: Return response of action taken on order item
	Your System ->> Your System: Write detailed action into SOAP notes
{{< /mermaid >}}


