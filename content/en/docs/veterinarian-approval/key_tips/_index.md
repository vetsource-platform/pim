---
title: "Important Implementation Points"
linkTitle: "Important Implementation Points"
weight: 170
mermaid: true
date: 2024-08-01 

description: >
  Helpful hints for implementing prescription management
---
You will probably first want to work on the [staff synchronization](https://vetsource-platform.gitlab.io/pim/docs/staff-management/). This is what keeps the providers in PIMS in sync with the provider records in Vetsource. For existing vetsource practices when the integration is enabled you will want to map the vetsource vets to the PIMS vet records. To do this you can perform a [GET staff call](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/get-staff) on the practice. During development we recommend that you work with your designated TPM to set some vet records up in the vetsource system so you can prepare to do this. During the initial [GET call](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/get-staff) there will be records with no staffId and you will want to get that set properly as that is required for any prescribing in the vetsource system. Using the staffApiId (which is a unique id in vetsource) you can perform a [PUT call](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/put-staff) to update common records. After the initial synchronization you would use the [PUT with the staffId](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/put-staff) from your system to do updates. You may want to periodically check for new staff records in vetsource as a clinic could add them through our system. You can add new veterinarians to our system using the [POST endpoint](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/post-staff-v2).

For the Approval queue you will need three endpoints, the [GET orders awaiting approval](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/get-orders-awaitingapproval) API, the [POST approve order items API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/post-approve-rx), and the [POST deny order items API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/post-deny-rx).

Use the GET to return the list and the others to take actions against the items.

* itemType is used in the approve and deny endpoints.
* The denialReasons in the GET are dynamic and can change so don't hardcode them for your denial UI. They are different for e-commerce, scriptright, and 3rdPartyOrigination orders. If the selected item is simply "Other" you could present a free-form text box and allow for entry up to 255 characters. 
* Use the [single item retrieval call](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/get-orderitem-awaitingappral) to return the dynamic values for displaying in your approval or denial interface without having to store locally.
* Exception messages are extremely important here. Display them to your end user so they can make the corrections to fix their issues without calling into support. 
* Use the usageInstructions to default to your provider with the ability for them to edit as needed. If these are available they are what is suggested by the manufacturer.
* Use the defaultExpirationDate to default the expiration date with the ability for the provider to override.
* Use the default refills to populate the refills field, but allow the provider to change the amount, but note they cannot go over the maximum refills
* The staffID of the provider is what is required for approval. Passing in a staffID not in vetsource will cause an exception that you will want to catch and correct. 
* For items where the maximumRefills is 999 you can pass in PRN. These are diet items and the veterinarian will want them to be available as needed without falling back into the approval queue for the year.
* In some cases for certain compounded medications, if the orderitem comes back in the GET call with isClinicalRationaleRequired = true, you will need to present a selection list of the clinicalRationales found for the provider to select appropriately. See the [Clinical Rationale example](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/get-orders-awaitingapproval) 200 codes for an example response and the [Clinical Rational Payload Example for the approval API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/rx_approval/post-approve-rx).
* For items where controlledSubstance = true you may want to display this messaging to the veterinarian so they know what to expect when approving controlled substances.
```angular2html
Additional actions for controlled substances
After approving this prescription, additional confirmation is needed to dispense your medication:
* Within the next few hours, a fax will be sent to your practice with a written prescription that you must sign and return
* If you prefer verbal confirmation, call (877)738-8883 to speak to a pharmacist
* To accept shipment, a signature from a legal adult (over the age of 21) is required

The order will be cancelled if confirmation isn't received within 3 days
```
* It is important to test orders that come in for clients that are not yet associated to your PIMS and who may have no association. These clients may need to be matched up using the Client Management API's so be sure to test scenarios that require non-associated clients/pets that perhaps came in through the practice's ecommerce storefront hosted by vetsource.