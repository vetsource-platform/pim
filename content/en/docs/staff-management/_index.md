---
title: "Staff Management"
linkTitle: "Staff Management"
weight: 400
mermaid: true
date: 2024-05-22


description: >
  Retrieve, update and add staff information with these API's.
---

Most veterinary practice managers have the annoying task of updating staff records in both the PIMS and in the vetsource application. This can lead to errors with duplicate accounts and prescription mismatches. Implement these API's and drastically improve the user experience for your veterinary clients by letting your PIMS be the source of truth for staff management.

This is a very important step in making a successful integration. Note for existing Vetsource practice customers that are turning on the integration you are creating they will already have staff records used to approve and deny orders and create recommendations. You will want to retrieve their user list and map those to your own.

### Key User Flows
* Manage duplicate staff records from within the practice management system 
* Synchronize account staff and veterinarian records without leaving the practice management system

### Key Features
* Locate Practice Home accounts that might not be synchronized with records found in the practice management system preventing timely responses to prescription approvals
* Create new staff and veterinarian records from practice management data

### Record Retrieval
Use this API to retrieve staff records from Vetsource for the purpose of synchronizing those records with your system.

[Staff - GET API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/get-staff)

{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    Veterinary User ->> Your System: Click button or open tab
    Your System ->> Vetsource: Invoke API
    Vetsource ->> Your System: Filter, sort and list Response
    Your System ->> Veterinary User: View List
{{< /mermaid >}}

### Record Entry
Use this API to add new staff records in vetsource from your system.

#### Important notes: 
1. If you do not return all the existing staff during onboarding you may create duplicates by inserting your own staff records. You need to synchronize the staff for existing vetsource implementations.

[Staff - POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/post-staff-v2)

{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    Veterinary User ->> Your System: Click button to add
    Your System ->> Vetsource: Invoke API
    Vetsource ->> Your System: Consume Response and add to list
    Your System ->> Veterinary User: View List
{{< /mermaid >}}

### Record Update
Use this API to update existing records from your system where there is a match between your users and the vetsource staff records.

#### Important notes:
1. Passing in null for the veterinarian pin will allow the pin to be preserved. If you overwrite the veterinarian's pin without letting them know it may prevent their ability to approve/deny prescriptions or pre-auth recommendations in Practice Home.
2. Emails are not necessarily the username for users in the Vetsource world. It is highly possible for some practices to have shared emails for staff.

[Staff - PUT API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/put-staff)

{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    Veterinary User ->> Your System: Click button to update
    Your System ->> Vetsource: Invoke API
    Vetsource ->> Your System: Consume Response and update list
    Your System ->> Veterinary User: View List
{{< /mermaid >}}

### Record Removal

[Staff - PUT API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/put-staff)

#### Important notes:
1. Removing a record is merely a change in the status to active/not active. The record will still exist but will not appear in Vetsource applications once a veteriarian or staff record is set to inactive.

### Record Removal Sequence
{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    Veterinary User ->> Your System: Click button to update
    Your System ->> Vetsource: Invoke API with "active" = false
    Vetsource ->> Your System: Consume Response and update status
    Your System ->> Veterinary User: View List
{{< /mermaid >}}