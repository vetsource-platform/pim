---
title: "Important Implementation Points"
linkTitle: "Important Implementation Points"
weight: 170
mermaid: true
date: 2024-05-31 

description: >
  Helpful hints for implementing staff management
---

* The key for both recommendations and approvals is for the veterinarian element to be equal to true. Non-veterinarians cannot approve prescriptions. 
* Be conscious of changes in the vetsource system as permissions are controlled for the staff users through this API. 
* PINS are the electronic signatures used to create prescriptions. Sending in null for the PIN on a PUT will cause it not to change. If you handle this wrong it may cause support escalations and delayed orders which will cause hospital frustrations.
    emails are not unique and can be used across multiple staff 
* In vetsource emails do not necessarily equal usernames 
* In the POST call we will throw an exception if you try to send in a staff record which matches an existing record on first and last name. We do know that there will be rare examples where a father/son with the same name could possibly exist but we have found this is the best way to preserve the integrity of the data. Once the staffId is set this will not be a problem and this can be handled by our support staff. 
* The staffId may be called the veterinarianId in some endpoints and used interchangeably.
