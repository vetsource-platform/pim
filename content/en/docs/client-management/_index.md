---
title: "Client Management"
linkTitle: "Client Management"
weight: 400
mermaid: true
date: 2024-12-30


description: >
  Manage client, animal and address information
---

### Key User Flows
* Manage duplicate client and pet records found in a practice’s e-commerce platform
* Synchronize client and patient account records with the practice management system
* Synchronize client, pet and address records from PIMS to Vetsource

### Key Features
* Locate e-commerce and Practice Home accounts that might not be synchronized with records found in the practice management system
* Create new Vetsource records from practice management data allowing the practice to take advantage of email campaigns for their e-commerce platform
* Keep pet weights synchronized with the latest updates in the practice management system ensuring fewer pharmacy cancellations


### Key Notes for Developers

* Client records are unique to each practiceApiId and are not shared between practices (including practices that might be sharing a PIMS)
* Pets have a 1 to 1 association with a client in the vetsource ecosystem. At present, we have no concept of shared ownership or pet care. 
* You cannot move a pet from one client to another and preserve their prescriptions or order history. Both stay with the original pet parent. 
* Care needs to be made when deactivating clients and pets as such will deactivate prescriptions and autoships. It is highly advisable before doing so that a clear warning and audit history is made as this could cause inconvenience to the clinic and pet owner if done by mistake. 
* The keys from the client to power recommendations will be the clientId and the petId. These are the ID's from the Practice Management System that should be stored as externalId's in Vetsource to show the association. Using the POST API below will set them.
* If a responseCode of 429 is received note the response message as it might instruct to try again.
* When sending in a POST (see below) we first check to see if matching records might exist which have unassociated PIM Identifiers. If First name, last name, email, phone, and other critieria exist and perfectly match we will associate your ID to the existing record. We are constantly trying to improve our matching logic.
* Things to take into account are initials, special characters, extra info in the last name (some examples are Sr. III, Phd, DVM) etc. These could cause a matching issue and create an unintended duplicate account requiring an increased workload for support teams.

### Managing Client/Patient Records (Simple Positive Flow)
{{<mermaid>}}
sequenceDiagram
participant Client
participant Your System
participant Vetsource
Client ->> Your System: Enter Client and Address Information
Your System ->> Vetsource: Invoke Client POST API
Vetsource ->> Your System: Return 200 OK
Client ->> Your System: Enter Pet Information for Client
Your System ->> Vetsource: Invoke Pet POST API
Vetsource ->> Your System: Return 200 OK
{{< /mermaid >}}


### Adding clients/addresses to the Vetsource System
[Client POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/post-v2-client)

If the PIMS customer and animal data is dataloaded using a data aggregator such as SyncVet the clients are loaded the previous night. In the event that a client or pet is seen for the first time (walk-in or emergency visit usually). In that event the POST API can be used to insert the missing client.

### Updating client records in the vetsource system
[Client PATCH API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/patch-v2-client)

### Deleting (soft) client records in the vetsource system
[Client DELETE API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/delete-v2-client)

Care needs to be made when deactivating clients and pets as such will deactivate prescriptions and autoships potentially affecting practice revenue and customer expectations. It is highly advisable before doing so that a clear warning is given and an audit history is made as this could cause inconvenience to the clinic and pet owner if done by mistake.

### Reactivating client records when deleted by mistake
[Client POST Reactivate](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/post-v2-client-reactivate)

### Searching for clients
[Client GET API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/get-v2-client)

#### Potential Use Cases
* Partner support teams could use to locate duplicate client issues
* PIMS who want to give the ability to practices to handle mapping and managing duplicate client accounts
* Third Party Applications using the web-shopping-apis or creating recommendations could make use of the search API to obtain necessary PIMS keys.
* Clients can come in through the hosted ecommerce site without a PIMS ID associated. It may be important to establish this relationship by retrieving those users and importing them into your PIMS if no match is found and updating the record with the proper ID.

### Handling Duplicate Clients
When synchronizing records from the PIMS to Vetsource there are occasions where duplicate records might have come in from sources like the practice's Vetsource hosted ecommerce site or 3rd party prescription routing. In that case you might get a duplicate client error when attempting to match client's using the patch API. In that event you might wish to create a dialogue for the end user allowing them to manage clients in the vetsource application by checking their activity history. Use this API to do so:
[Client Activity History API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/post-v2-client-activity-history)

#### Managing Duplicate Client Records 
An API messages similar to the following may be received:

```
Duplicate client was found. Client found: 3914ef27-c47f-4682-82cc-718b5fd7fc52"
```
It is recommended to capture and handle such messages by producing the appropriate prompts to the end user to allow them to make decisions to handle duplicate records.

{{<mermaid>}}
sequenceDiagram
participant Client
participant Your System
participant Vetsource
Client ->> Your System: Enter Client and Address Information
Your System ->> Vetsource: Invoke Client POST API
Vetsource ->> Your System: Return 400 Bad Request with Duplicate Client Found
Your System ->> Vetsource: Invoke Client GET with API ID of found Client(s)
Your System ->> Vetsource: Invoke Client GET Activity History with API ID
Your System ->> Client: Display Duplicate Client information with choice
Client ->> Your System: Select Client to Associate to PIMS
Your System ->> Vetsource: Invoke Delete API for Client not Associated
Vetsource ->> Your System: Return 200 OK
Your System ->> Vetsource: Invoke Client Patch for Client Selected
Vetsource ->> Your System: Return 200 OK
{{< /mermaid >}}

### Finding a client's pets in the Vetsource System
[Pet GET API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/get-v2-client-pet)

### Adding pets to the Vetsource System
[Pet POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/post-v2-pet)

If the PIMS customer and animal data is dataloaded using a data aggregator such as SyncVet the clients are loaded the previous night. In the event that a client or pet is seen for the first time (walk-in or emergency visit usually). In that event the [POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/post-v2-pet) can be used to insert the missing pet or animal.

### Updating Pet information
[Pet PATCH API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/patch-v2-client-pet)

### Deleting Pet information
[Pet DELETE API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/delete-v2-UNIQUE_PRACTICE_API_ID-client-UNIQUE_CLIENT_PIMS_ID-pet-UNIQUE_PET_PIMS_ID)

### Reactivate a Deleted Pet
[Pet Reactivate POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/post-v2-pet-reactivate)

### Mark a Pet Deceased
[Pet Decease POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/post-v2-pet-deceased)

### Handling Duplicate Pets
[Pet Activity History POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/post-v2-pet-activity-history)

### Find a client's addresses
[Address GET API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/get-v2-client-address)

### Create a client's address
[Address POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/post-v2-client-address)

### Update a client's address
[Address PATCH API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/patch-v2-client-address)

### Delete a client's address
[Address DELETE API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/client/delete-v2-client-address)

#### Tips
* You must have at least one default billing and one default shipping address set, or you will get an exception when trying to delete an address