---
title: "API"
linkTitle: "API"
type: swagger
weight: 300
date: 2024-12-30
menu:
  main:
    weight: 300
---

{{< swaggerui src="/pim/api/openapi.yaml" >}}
