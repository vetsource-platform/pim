---
title: "Pay-By-Link"
linkTitle: "Pay-By-Link"
weight: 700
mermaid: true
date: 2024-10-01 

description: >
  Generate Adyen hosted Pay-By-Link for practices enrolled as sub-merchants in Vetsource Payments
---

### API
[POST Get Payment Link API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/payments/post-paymentlink)

### Key User Flows
1. Practice has a chat tool and wish to take payments for a consultation
2. Practice's client has a scheduled surgery and the practice wants to take a deposit to guarantee no loss of payment in case of no show.

### Requirements
1. Practice must be enrolled in Vetsource Payments to use this functionality
2. Practice must have and active PIMS data connection

### Optional
As a partner implementing this feature you may want to set up a way to obtain updates for when a payment has been received through the link. 

Our recommendation is that the call back URL be unique for each request https://{{partner-domain}}.com/updates/{{someuniquevalue}} or https://{{partner-domain}}.com/updates?ref={{some-unique-value}}

We will make a PUT call to those endpoints and the payload will be like:

```
{
"reference" : "882318",
"status" : "PAID"
}
```

In this context the reference will be the same reference from the reference when you created the link:

```
{
"paymentlink" : "adyen.com/AS123SHE",
"reference" : "882318"
}
```
Also note that if a pay-by-link has been canceled you could receive a status to the callback url of "CANCELED"

### User Flow

{{<mermaid>}}
sequenceDiagram
participant Veterinary User
participant Your System
participant Vetsource
participant Veterinary Client
participant Adyen
Veterinary User ->> Your System: Click button to generate request for payment
Your System ->> Vetsource: Invoke POST API with amount and callback URL
Vetsource ->> Your System: Return URL
Your System ->> Veterinary Client: Display URL to take payment
Veterinary Client ->> Adyen: Make Payment
Vetsource ->> Your System: Send PUT API to your callback URL
Your System ->> Veterinary User: Update with status
Adyen ->> Vetsource: Handle Payout
{{< /mermaid >}}
