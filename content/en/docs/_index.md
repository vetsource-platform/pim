---
title: ""
linkTitle: "Documentation"
weight: 200
menu:
  main:
    weight: 200
---


### The Vetsource PIMS Integration Toolkit

The PIMS Integration Toolkit enables you to quickly and easily add veterinarian facing solutions to your Practice Information Management System (PIMS).

With this Toolkit, you can:

* Log directly into the ScriptRight application to begin writing a prescription for a specific pet.
* Give Veterinarians direct access to begin approving and denying prescriptions that are in the queue.
* Give Practices quick access to expedite the checkout process for ScriptRight orders.

In order to use this Toolkit, you will need to have access to Practice, Client, Address, and Pet information from the PIMS.

