---
title: "Automated Email Drip Campaign"
linkTitle: "Automated Email Drip Campaign"
weight: 180
mermaid: true
date: 2023-09-08 

description: >
   How the drip campaign works
---

View email examples here: [https://pages.vetsource.com/ScriptShare-Notifications.html](https://pages.vetsource.com/ScriptShare-Notifications.html)

![Drip Campaign](../../images/ScriptShare/DripCampaign.png)

