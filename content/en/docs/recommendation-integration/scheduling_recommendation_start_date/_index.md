---
title: "Scheduling Recommendation Start Date"
linkTitle: "Scheduling Recommendation Start Date"
weight: 150
mermaid: true
date: 2023-09-08 

description: >
  Set your recommendations to start in the future.
---

Also provided in the API is an option to begin the campaign on a designated date in the future.  This is most commonly used when the veterinarian wants to send the patient home with the first dose dispensed in-clinic, but would like to invite the client to purchase subsequent orders online. Typical use cases are:

*   chronic medications,
*   allergy medications
*    flea/tick
*    heartworm therapies.

This saves the clinic time and reduces risk, since every refill dispensed in the clinic equates to 21 steps across 5 departments.

The recommendation may be scheduled for delivery on a future date. If this field is not sent, the recommendation campaign will commence within 5 minutes of the request.

Simply adding an element like the following to your JSON Payload will enable this feature:
```
 "scheduleSendDate": "2023-07-16"
```

