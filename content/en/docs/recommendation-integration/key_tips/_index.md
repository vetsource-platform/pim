---
title: "Important Implementation Points"
linkTitle: "Important Implementation Points"
weight: 170
mermaid: true
date: 2024-05-31 

description: >
  Helpful hints for implementing recommendations
---

Key details to keep in mind:

* You will probably first want to work on the staff synchronization. This is what keeps the providers in PIMS in sync with the provider records in Vetsource. For existing vetsource practices when the integration is enabled you will want to map the vetsource vets to the PIMS vet records. To do this you can perform a [GET staff call](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/get-staff) on the practice. During development we recommend that you work with your designated TPM to set some vet records up in the vetsource system so you can prepare to do this. During the initial [GET call](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/get-staff) there will be records with no staffId and you will want to get that set properly as that is required for any prescribing in the vetsource system. Using the staffApiId (which is a unique id in vetsource) you can perform a [PUT call](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/put-staff) to update common records. After the initial synchronization you would use the [PUT with the staffId](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/put-staff) from your system to do updates. You may want to periodically check for new staff records in vetsource as a clinic could add them through our system. You can add new veterinarians to our system using the [POST endpoint](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/staff/post-staff-v2).
* If you want to recommend products you will need to use one of the catalog endpoints depending on your desired implementation. One lets you search a practice's catalog requiring the most up-to-date products. The other allows you to download and store our products so you could map them locally.  We would recommend you start by using the [search endpoint](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/catalog/get-catalog-practiceSearch) to find products.
* Make note of the example payloads provided in the documentation as that will instruct what you need to populate for your scenarios.
* A pre-authorized recommendation creates a prescription in our system before the client actually purchases the order in the hosted e-commerce platform.
* A recommendation sent without prescription information which gets purchased by the pet owner will fall into the approve/deny queue to require a prescription before it will go into fulfillment.
* While not required, the personalNote field is recommended to be used to improve conversions. Note that this is a personal note to be sent to the client. Some veterinarians have misunderstood this file to be a personal note to themselves. You may want to create templates or suggested verbiage in your UI.
* If the suppressEmailCampaign is not set to true we will send a practice-branded campaign to the client until they purchase or they receive three emails. They do have the ability to opt-out of any particular recommendation campaign and that will not prevent a future send.
* If the suppressEmailCampaign is set to true you would need to use the web-shopping-api endpoint to retrieve the client's specific recommendation URL. Potential uses might be a push notification through a client engagement application.
* In the catalog API used to select the product for recommendation be sure to make use of the defaultExpirationDate and usageInstructions (which you should default if we have them.)