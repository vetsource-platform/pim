---
title: "Select a Product to Recommend"
linkTitle: "Select a Product to Recommend"
weight: 130
mermaid: true
date: 2023-09-11 

description: >
   Explains how to search for and select a product to recommend.
---

This API allows users to interact with a specific clinic’s Vetsource product catalog via a search mechanism, which will give real-time interaction with the practice’s ecommerce catalog, taking into consideration its buyable products, backorder status, and important information like pricing and promos.

This allows you to insert a product search tool directly into your application for the purpose of creating a recommendation or giving the user the ability to check product availability or price.

* It takes only three (3) characters of entry to retrieve valid products
* Automatically excludes products the clinic has chosen not to carry or are not currently buyable.
* Purposefully built with veterinarians in mind, returning results in the way they think of therapies and medications allowing them to search by:
    * Drug name
    * Generic
    * Ingredient
    * Condition
    * Benefits
    * Nutrition lifecycle
    * Product line
    * Problem

![Select a Product Example](../../images/ScriptShare/select_product1.png)

[Product - GET API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/catalog/get-catalog-practiceSearch)
