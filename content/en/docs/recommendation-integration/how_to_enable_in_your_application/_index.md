---
title: "How to Enable"
linkTitle: "How to Enable"
weight: 191
mermaid: true
date: 2023-09-11 

description: >
   Instructions on how this might be enabled in your application.
---

### Recommendation Creation Sequence 
{{<mermaid>}}
    sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    participant Client
    Veterinary User ->> Your System: Search for product
    Your System ->> Vetsource: Make Catalog API Call
    Vetsource ->> Your System: Return matching products
    Your System ->> Veterinary User: Display products to Select
    Veterinary User ->> Your System: Select patient, product and qty
    Veterinary User ->> Your System: (Optional) Enter personal note
    Veterinary User ->> Your System: (Optional) Select future date to start campaign
    Veterinary User ->> Your System: Click button to create recommendation
    Your System ->> Your System: Create JSON String with Vetsource SKU Id
    Your System ->> Vetsource: Send JSON string in API Call
    Vetsource ->> Your System: Return JSON response
    Your System ->> Veterinary User: Data displayed
    Vetsource ->> Client: Send email drip campaign
{{< /mermaid >}}


[Catalog - GET API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/catalog/get-catalog-practiceSearch)

[Recommendation - POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/recommend/post-recommendations)

[Recommendation - GET API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/recommend/get-recommendations)

[Recommendation - DEL API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/recommend/delete-recommendation)
