---
title: "Updating & Disabling a Recommendation Campaign"
linkTitle: "Updating & Disabling a Recommendation Campaign"
weight: 160
mermaid: true
date: 2023-09-08 

description: >
   Give your users the flexibility to control recommendation campaigns.
---

There are instances where the veterinarian may need to update a recommendation after the campaign has commenced.  This endpoint allows you to update the following fields:

*   Email address - If the address on file in the PIMS is not current and the client has provided an updated one
*   Phone
*   Personalized note
*   Status - Allows you to halt, or unsubscribe, a pet owner from a specific recommendation, but will allow future recommendations to be sent.  Common scenarios for this may be a pet owner purchases a different product and no longer wants to receive these emails, the campaign was inadvertently sent to the wrong client, or a pain medication recommendation for a patient nearing end-of-life where the animal is euthanized before the pet owner completes their purchase.
