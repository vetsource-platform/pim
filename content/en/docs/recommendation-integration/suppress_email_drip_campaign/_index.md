---
title: "Suppress Email Campaign"
linkTitle: "Suppress Email Campaign"
weight: 190
mermaid: true
date: 2023-09-08 

description: >
   Create a recommendation, but suppress sending the drip campaign.
---

Partners may want to control how the recommendation is sent to the pet owner.  This parameter allows you to create the recommendation and manage your own notification method.

Alternatively, you can retrieve the unique client URL and insert that into your application, such as having a recommendation menu in your pet owner application or inserting the link into your application’s chat session.

To enable this feature simply add the following line inside the JSON Payload:

```
   "emailCampaignEnabled": true,
```