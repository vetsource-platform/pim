---
title: "Key User Flows"
linkTitle: "Key User Flows"
weight: 170
mermaid: true
date: 2023-09-08 

description: >
  Potential user flows to explain how recommendations work.
---

*   Pet owner visits a DVM. DVM orders a test that will take some time to receive the results. The pet owner goes home. Based on the test results the DVM wishes to suggest prescription medication for the pet. The DVM recommends a product to the pet owner by creating a recommendation in their PIMS, using the native workflows the PIMS partner has created using these APIs.
*   Patient comes to the clinic for an appointment. Vet creates prescription with a cadence and quantity for medication and dispenses first fill locally from clinic stock. Patient goes home with medication.
    *   DVM creates a recommendation scheduled to send the email on a future date, which then reminds the pet owner to purchase the next fill from their online pharmacy.
    *   Alternatively, a veterinary technician creates a recommendation for a prescription item they know the general practitioner has talked to the client about during a visit.  The technician cannot create a prescription so the recommendation is sent without authorization.  The pet owner purchases the item and it is routed to the Orders Awaiting Approval queue for the veterinarian to review and authorize.
*   Client calls their DVM for a prescription renewal. The DVM creates a recommendation with authorization (signed). The pet owner receives the recommendation email and completes the checkout and purchase on the online storefront.  The item drops directly into fulfillment.
*   DVM creates a prescription for medication with refills. All refills are used by the pet owner and the prescription has expired. Pet owner calls DVM for a refill. DVM creates a recommendation with authorization to allow the pet owner to continue on the prescribed therapy.
*   Pet Owner initiates a chat for a virtual consultation with a veterinarian. DVM creates a recommendation and inserts the link for the client to purchase from the online storefront in the chat. Pet Owner completes checkout.

![Recommendations Mobile Example](../../images/ScriptShare/OneProduct.png)

![Recommendations Desktop Example](../../images/ScriptShare/Recommendations_Desktop.png)