---
title: "Common Implementation Strategies"
linkTitle: "Common Implementation Strategies"
weight: 120
mermaid: true
date: 2023-09-08 

description: >
  Suggestions on how to implement successful recommendation offerings
---

*   Follow-up to annual check-up (diets, supplements in particular).
*   Attempts to improve continuation of care (preventatives).
*   Improve compliance when there is a gap or lapse in a master treatment or drug plan
*   Transition chronic treatments (e.g., NSAIDS) to home delivery to save staff time and reduce clinical risk.
*   Client has left the practice after a visit without purchasing a verbally recommended product (80% leave without buying the prescription diet suggested; 60% leave without the prescribed drug).
*   Respond to competitive online pharmacy requests.
*   An outreach opportunity to stay engaged with patients/clients between office visits, perhaps for a toy, chew or treat.
*   Last resort - nothing else works with this client.

