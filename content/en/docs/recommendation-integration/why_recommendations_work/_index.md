---
title: "Why Recommendations Work"
linkTitle: "Why Recommendations Work"
weight: 110
mermaid: true
date: 2023-09-08 

description: >
   This section explains why recommendations are effective.
---

*   Recommending a therapy plan for their patients to ensure a standard of care is a core part of the veterinarian's job.
*   Practices want to be able to recommend products to their clients so they can review and purchase them at their own convenience.
*   Recommendations are a valuable asset in the clinic's toolkit as it provides an additional touchpoint to the client to remind them what their vet says they need to provide the best care for their pet.
*   A timely recommendation can help clinics compete with online pharmacies and keep revenue in-house.
*   Seventy-five percent of pet owners cite their veterinarian's recommendation as the deciding factor on the therapy they purchase.

This endpoint assumes the client and patient already exist in the Vetsource ecosystem. Typically client and patient records are inserted on a nightly process. This process reads/copies client and pet information from the Practice Information Management System (PIMS) into Vetsource to simplify and facilitate the creation of orders and prescriptions.

In the instance that the client is new to the clinic as of today, you can use the create client/pet endpoints to add them into Vetsource as a prerequisite to creating a recommendation.
