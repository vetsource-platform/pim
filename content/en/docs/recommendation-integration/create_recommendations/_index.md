---
title: "Create Recommendations"
linkTitle: "Create Recommendations"
weight: 100
mermaid: true
date: 2023-09-08 

description: >
   Get proactive with recommendations for prescription, OTC, and nutritional therapies and stay ahead of competing online pharmacies.
---


This endpoint allows you to keep your therapy recommendations top of mind and encourages clients to purchase the products their pets need.

*   You can recommend one product, several products for the same pet, or multiple products for different pets under the same client.
*   It's convenient for your clients as they need only complete check-out
*   It’s convenient for your clinic as there is no need to collect payment
*   You’ll enjoy improved compliance and increased revenue.
*   One of four email campaigns results in a purchase with a $26K average per year potential revenue, resulting in 100% incremental revenue for your practice.
