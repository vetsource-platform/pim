---
title: "Add a Personal Note"
linkTitle: "Add a Personal Note"
weight: 140
mermaid: true
date: 2024-08-02 

description: >
  Add personal notes to your recommendations to get a better conversion rate.
---

An option to include a personalized note from the vet or clinic can be utilized as a reminder to the client of what was discussed during the office visit, or to reinforce additional usage instructions. This will display as additional comments in the email campaign as well as on the pet owner’s recommendation tray on the online storefront. 

Implementation Notes
* Create templates which autofill details like the client name, pet name, product name to save the Provider time.
* While this is an optional field, this field is known historically to increase conversions which will ultimately benefit your client in increased sales.
* Tell the end user what the field is for in an info hover.

Simply adding an element like the following to your JSON Payload will provide this feature:

```
 "personalNote": "Here is the product we talked about giving to Fido to clear up his minor skin irriitation condition when we met in the office earlier today. Thanks again for coming in. - Dr. Lewis"
```
