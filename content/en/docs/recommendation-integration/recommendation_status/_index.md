---
title: "Recommendation Status"
linkTitle: "Recommendation Status"
weight: 155
mermaid: true
date: 2023-09-11 

description: >
   This endpoint allows you to check the purchased status for a recommendation.  Veterinarians often want to know what actions the client has taken, or has not.
---

  Common workflows you can implement:
* Include purchased and shipped recommendations in the patient medical records, so that veterinarians can print the Rx or include the prescription details in a full medical record sharing between providers.
* Display un-purchased items in the patient history to assist in client/patient communication and care.  
* Connect subsequent orders shipped to the original prescription with our GET prescription endpoints
