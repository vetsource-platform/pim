---
title: "eCommerce Single Sign On"
linkTitle: "eCommerce Single Sign On"
weight: 400
mermaid: true
date: 2022-12-14


description: >
  Give a client instantaneous access to their ecommerce experience and online prescription information.
---

[Client Single Sign-On POST API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/web-shopping-api/post-single-sign-on)

### Single Sign On Sequence
{{<mermaid>}}
sequenceDiagram
    participant Client
    participant Your System
    participant Vetsource
    Client ->> Your System: Click button
    Your System ->> Vetsource: Invoke API
    Vetsource ->> Your System: Return URL's
    Your System ->> Client: Select location from response and present URL in browser
{{< /mermaid >}}

In order to create a seamless customer experience between your application and Vetsource's application, you can automatically sign clients into their practice’s eCommerce site (hosted by Vetsource) and place them into any one of the following destinations.

### Address and Payment Method Management

Clients can easily manage their address information and payment methods for their veterinary practice's eCommerce shopping site.

### Patient Management

Patient information is key to providing the correct data to attending physicians and Vetsource's pharmacy.

### Ecommerce Shopping
Ecommerce shopping allows the client to browse the practice's online shopping products and make purchases, set up auto-shipments on a recurring schedule.

Veterinarians can also recommend products to their customers allowing them to purchase through the practice's ecommerce shopping site.

With the Single Sign-On API the client will also have easy access to online prescription and order status information.
