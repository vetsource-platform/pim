---
title: "Real-time Compliance History"
linkTitle: "Compliance History"
weight: 400
mermaid: true
date: 2023-07-21


description: >
  Retrieve current prescription status and transaction statuses.
---

To provide the best care, veterinarians need access to the complete medical record. With these API’s all home delivery history for a pet parent and an animal’s current status and history can be retrieved without having to leave your application and swivel chair into another.

Further application could be observing order status, and providing tracking information for orders that have been shipped.

**Note: These API's should be ran on an as needed basis only when the client or patient records are accessed. They are not to be used to update all client records on a schedule. Misuse may result in data throttling.**

### Prescription Information Retrieval for Patient

[Compliance Rx GET-API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/compliance/get-prescriptions-pet)

{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    Veterinary User ->> Your System: Click button
    Your System ->> Vetsource: Request Rx info by patient PIMS ID
    Vetsource ->> Your System: parse json
    Your System ->> Veterinary User: present home delivery Rx history
{{< /mermaid >}}


### Order Information Retrieval

[Compliance Order GET-API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/compliance/get-orders-client)

{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    Veterinary User ->> Your System: Click button
    Your System ->> Vetsource: Request order info by client PIMS ID
    Vetsource ->> Your System: parse json
    Your System ->> Veterinary User: present home delivery order history
{{< /mermaid >}}

### Get Specific Prescription Information For Specific Order Item

[Rx GET-API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/compliance/get-prescription-by-Api-Id)

{{<mermaid>}}
sequenceDiagram
participant Veterinary User
participant Your System
participant Vetsource
Veterinary User ->> Your System: Click button to see prescription info for order item
Your System ->> Vetsource: Request Rx info by specific Rx API Id
Vetsource ->> Your System: parse json
Your System ->> Veterinary User: present current Prescription Status
{{< /mermaid >}}
