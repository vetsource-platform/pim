---
title: "Entire Product Catalog"
linkTitle: "Entire Product Catalog"
weight: 150
mermaid: true
date: 2023-09-08


description: >
  Instructions on how to download an entire product catalog for a particular practice.
---

In the event that our product search API might cause performance issues or that you would like to offer the ability to map internally invoiced items to Vetsource's product offering the following option is available so you can obtain an entire product catalog.

### Get Practice's Entire Catalog via API (File is large)
{{<mermaid>}}
sequenceDiagram
participant Your System
participant Vetsource
Your System ->> Vetsource: Make API call to retrieve entire catalog
Vetsource ->> Your System: Returns all products in practice's catalog
{{< /mermaid >}}

[Catalog - GET API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/catalog/get-catalog-export)