---
title: "Key Features"
linkTitle: "Key Features"
weight: 120
mermaid: true
date: 2023-09-08


description: >
  Explains the key features of the catalog API's
---

*   Product details provided directly from the manufacturer gives veterinarians an easy way to check active ingredients, usage instructions, side effects and drug and food interactions, ensuring they can make appropriate clinical decisions.
*   Default prescription usage instructions for flea/tick and heartworm products and many others
*   Active promotion blurbs at the item level
*   URL provided for vet consumer to be able to view product item on their shopping site in the response
*   URL to the image on the eCommerce product page in the response
*   Back-ordered status and estimated date returning to inventory if available
*   Replacement products for manufacturer discontinued items
*   UPC codes for many products

\*Currently excludes our compounding partner, Wedgewood Pharmacy, catalog.

