---
title: "Search On Specific Catalog Item"
linkTitle: "Search On Specific Catalog Item"
weight: 120
mermaid: true
date: 2023-09-08


description: >
   In order to fully enable an embedded feature like product recommendation, we are providing a way for API users to interact with a practice's ecommerce catalog via a search mechanism, which gives real-time interaction with a catalog, it's buyable products, backorder status, exclusions and important information like pricing.
---

**Searching for a Particular Product**

The Vetsource systems (like recommend product) require a valid sku from a practice's available ecommerce catalog. By searching through the catalog query api it is easy to retrieve products for recommending.

This API allows users to interact with a specific clinic’s Vetsource product catalog via a search mechanism, which will give real-time interaction with the practice’s ecommerce catalog, taking into consideration its buyable products, backorder status, and important information like pricing and promos.

This allows you to insert a product search tool directly into your application for the purpose of creating a recommendation or giving the user the ability to check product availability or price.

* It takes only three (3) characters of entry to retrieve valid products
* Automatically excludes products the clinic has chosen not to carry or are not currently buyable.
* Purposefully built with veterinarians in mind, returning results in the way they think of therapies and medications allowing them to search by:
    * Drug name
    * Generic
    * Ingredient
    * Condition
    * Benefits
    * Nutrition lifecycle
    * Product line
    * Problem

![Select a Product Example](../../images/ScriptShare/select_product1.png)

### Product Search via API
{{<mermaid>}}
sequenceDiagram
    participant Veterinary User
    participant Your System
    participant Vetsource
    participant Client
    Veterinary User ->> Your System: Enter more than 3 characters 
    Your System ->> Vetsource: Create URL with product query parameter
    Vetsource ->> Your System: Return Matching Products in JSON format
{{< /mermaid >}}

[Product - GET API](https://vetsource-platform.gitlab.io/pim/docs/openapi/#/catalog/get-catalog-practiceSearch)


