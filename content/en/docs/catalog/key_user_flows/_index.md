---
title: "Key User Flows"
linkTitle: "Key User Flows"
weight: 110
mermaid: true
date: 2023-09-08


description: >
  Explanation of the flows that users might take to utilize the practice catalog.
---

Partners may want to create recommendations directly in the PIMS by mapping their in-clinic inventory or prescription information to the equivalent Vetsource product SKU. Mobile application providers specializing in veterinarian and pet health tools want to manage recommendations to improve pet care. We provide the entire Vetsource’s product catalog for a specific practice to be available in your own local product list.\*

*   Suggest online purchases to transition certain medications from in-clinic refills to home delivery.
*   Suggest a refill when the pet owner purchases the 3-pack of a flea/tick/heartworm and they are coming due or past due a fill.
*   Send a prescription recommendation when the client leaves the appointment without purchasing the nutrition or medication that was discussed.
*   Send a recommendation when a gap in care is detected.
*   Display related product details such as ingredients, possible side effects and benefits in the prescription management workflows.
*   Display product images for at-a-glance visual representation and to reduce common prescribing errors.
